var WooCommerceAPI = window.WooCommerceAPI || {};

// Augment the object with modules that need to be exported.
// You only need to require the top-level modules, browserify
// will look for any dependencies and bundle them inside one file.

WooCommerceAPI.WooCommerceAPI = require('woocommerce-api');


// Add to the global namespace

window.WooCommerceAPI = WooCommerceAPI;