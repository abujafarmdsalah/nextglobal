import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController,  MenuController, ToastController, AlertController, LoadingController, Events } from '@ionic/angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public onLoginForm: FormGroup;
  public token;
  username: string;
  password: string;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    public events: Events,
    public storage: Storage,
    //public navParams: NavParams, 
    public http: Http
  ) {
    this.username = "";
    this.password = "";
     }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
     this.token = JSON.parse(localStorage.getItem("userData"));
    //console.log(token);
    if (this.token!=null) {
      this.navCtrl.navigateRoot('/home-results');
    }
  }

  ngOnInit() {

    this.onLoginForm = this.formBuilder.group({
      'username': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  async forgotPass() {
    const alert = await this.alertCtrl.create({
      header: 'Forgot Password?',
      message: 'Enter you email address to send a reset link password.',
      inputs: [
        {
          name: 'username',
          type: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: async () => {
            const loader = await this.loadingCtrl.create({
              duration: 2000
            });

            loader.present();
            loader.onWillDismiss().then(async l => {
              const toast = await this.toastCtrl.create({
                showCloseButton: true,
                message: 'Email was sended successfully.',
                duration: 3000,
                position: 'bottom'
              });

              toast.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  // // //
  goToRegister() {
    this.navCtrl.navigateRoot('/register');
  }

  goToHome() {
    this.navCtrl.navigateRoot('/home-results');
  }

  login(){
    var apiHost = 'http://www.nextglobalbd.com/wp-json';
    this.http.post( apiHost + '/jwt-auth/v1/token', {
        username: this.username,
        password: this.password
      } )
    .subscribe((res) => {
      console.log(res.json());

      let response = res.json();

      if(response.error){
        this.toastCtrl.create({
          message: response.error,
          duration: 5000
        });
        return;
      }

      if(response){
         localStorage.setItem('userData', JSON.stringify(response) )
         this.goToHome();
      }
    });


  }

}
