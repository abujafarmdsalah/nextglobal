import { Injectable } from '@angular/core';
import * as WC from 'woocommerce-api';

@Injectable()
export class WoocommerceService {

  Woocommerce: any;
  WoocommerceV2: any;

  constructor() {
    this.Woocommerce = WC({
      url: "http://www.nextglobalbd.com",
      consumerKey: "ck_246095c827435217fdfee1aa22d9a5fe8fbd5d63",
      consumerSecret: "cs_11ce72f5659a648e0fffc327d44589dd097a150a"
    });

    this.WoocommerceV2 = WC({
      url: "http://www.nextglobalbd.com",
      consumerKey: "ck_246095c827435217fdfee1aa22d9a5fe8fbd5d63",
      consumerSecret: "cs_11ce72f5659a648e0fffc327d44589dd097a150a",
      wpAPI: true,
      version: "wc/v2"
    });
  }

  init(v2?: boolean){
    if(v2 == true){
      return this.WoocommerceV2;
    } else {
      return this.Woocommerce;
    }
  }
}
